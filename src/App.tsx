import { useEffect, useState } from "react";
import { Container, List, FavoritesList } from "./styles";
import "./App.css";
import Digimons from "./components/Digimons";
import { IDigimon } from "./interfaces";
import { useFavoriteDigimons } from "./providers/FavoritesDigimons";

const App = () => {
  
  const { favorites } = useFavoriteDigimons();
  const [digimons, setDigimons] = useState<IDigimon[]>([]);
  const [error, setError] = useState<string>("");

  useEffect(() => {
    fetch("https://digimon-api.vercel.app/api/digimon")
      .then((response) => response.json())
      .then((response) => setDigimons([...response]))
      .catch((_) => setError("Algo deu errado!"));
  },[])
  
    return (
        <div className="App">
          <header className="App-header">
            <Container>
              <FavoritesList>
                <Digimons
                  digimons={favorites}
                  isFavorite={true}
                ></Digimons>
              </FavoritesList>
              <List>
                <Digimons
                  digimons={digimons}
                ></Digimons>
              </List>
            </Container>
          </header>
        </div>
    );
}

export default App;
