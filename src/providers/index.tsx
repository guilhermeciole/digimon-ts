import { ReactNode } from "react";
import { FavoriteDigimonsProvider } from "./FavoritesDigimons";

interface IProviderProps {
    children: ReactNode;
}

const Providers = ({children}: IProviderProps) => {
    return (
        <FavoriteDigimonsProvider>
            {children}
        </FavoriteDigimonsProvider>
    )
}

export default Providers;