import { createContext, ReactNode, useContext, useState } from "react";
import { IDigimon } from "../../interfaces";


interface IFavoriteDigimonsProviderProps {
    children: ReactNode;
}

interface IFavoriteDigimonsProviderData {
    favorites: IDigimon[];
    addDigimon: (digimon: IDigimon) => void;
    deleteDigimon: (digimon: IDigimon) => void;
}

const FavoriteDigimonsContext = createContext<IFavoriteDigimonsProviderData>({} as IFavoriteDigimonsProviderData)

export const FavoriteDigimonsProvider = ({children}:IFavoriteDigimonsProviderProps) => {

    const [favorites, setFavorites] = useState<IDigimon[]>([] as IDigimon[]);

    const addDigimon = (digimon: IDigimon) => {
        setFavorites([...favorites, digimon]);
    }

    const deleteDigimon = (digimonToBeDeleted: IDigimon) => {
        const newList = favorites.filter(digimon => digimon.name !== digimonToBeDeleted.name)
        setFavorites(newList);
    }

    return (
        <FavoriteDigimonsContext.Provider value={{favorites, addDigimon, deleteDigimon}}>
            {children}
        </FavoriteDigimonsContext.Provider>
    )
}

export const useFavoriteDigimons = () => useContext(FavoriteDigimonsContext);