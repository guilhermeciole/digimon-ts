import { ReactNode } from "react";
import { ButtonStyled } from "./styles";

interface IButtonProps {
    deleted?: boolean;
    children: ReactNode;
    onClick: () => void;
}

const Button = ({ deleted = false, children, onClick }: IButtonProps) => {
  return (
    <ButtonStyled isDeleted={deleted} onClick={onClick}>
      {children}
    </ButtonStyled>
  );
};

export default Button;
